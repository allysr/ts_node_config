📌 **CONFIGURANDO PROJETO** 
1. **npm init -y**
2. **npm i typescript -D**
3. **npx tsc -v**
4. **npx tsc --init**
5. **npm i ts-node -D**
6. **npm i nodemon -D**
    


 ▸**Mudar no tsconfig:**
    
- **“target”: “es2022**” Versão node.
- **“module”:”es2022”** Que versão de module usar.
- **“moduleResulation”:”Node16”** Quando importamos um modulo onde o TS vai buscar (dentro do node_modules, dentro da raiz da aplicação).
- **“sourceMap”: true** cria o arquivo sourcemap.
- **“outdir”: “./dist”** Em que diretorio jogar o código JS que foi gerado.
- **“rootDir”:”./src”** Diretório raiz da aplicação, onde os modulos TS serao buscados. Código fonte no SRC, tudo que compilar dele vai para o DIST.
    


 ▸**Para usar o “ts-node”:**
    
- Colocamos no final da tsconfig
- “ts-node: {     
    “esm”: true    
}
        


 ▸**Adicionamos no package Json:**
    
- “type”: “module” Para entender que estamos usamos os modulos ecmascript, quando houver extensoes  de modulos.


   
 ▸**Configurar no package Json "Scripts":**

- Eles rodam com **npm run nome**
- “start”: “ts-node src/index.ts”,
- “watch”:”nodemon src/index.ts”
